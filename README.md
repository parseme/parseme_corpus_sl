README
------
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Slovene, edition 1.3. See the wiki pages of the [PARSEME corpora](https://gitlab.com/parseme/corpora/-/wikis/) initiative for the full documentation of the annotation principles.

The present Slovene data result from an update and an extension of the Slovene part of the [PARSEME 1.1 corpus](http://hdl.handle.net/11372/LRT-2842).
For the changes with respect to the 1.2 version, see the change log below.


Corpora
-------
All annotated data come from the reference SUK training corpus for Slovene, which is available and described here:
* CLARIN.SI repository: http://hdl.handle.net/11356/1747
* Description: https://rsdo.slovenscina.eu/en/language-resources

Provided annotations
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. Details for specific columns:
* LEMMA (column 3): manually annotated
* UPOS (column 4): manually annotated (converted from XPOS)
* XPOS (column 5): manually annotated according to [JOS](http://nl.ijs.si/jos/josMSD-en.html) scheme
* UD dependency relations (column 6 and 7): first half is manually annotated (ssj500k-tag subset), while the second half (ssj500k-syn subset) was automatically parsed with [UDPipe 2](https://ufal.mff.cuni.cz/udpipe/2) slovenian-ssj-ud-2.10-220711 model
* MISC (column 10): includes information on spacing and named entities
* PARSEME:MWE (column 11): Manually annotated following the PARSEME Shared Task 1.1 [guidelines](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/). The following [VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/?page=030_Categories_of_VMWEs) are annotated: VID, LVC.cause, LVC.full, IAV, IRV.

Sentences from 1 to 8,641 have been annotated by two annotators per file. Sentences from 8,642 to 11,411 have been annotated by one annotator per file. In sentences 1 to 11,411, the MWE categories from the first version of the annotation guidelines (i.e. ID, LVC, VPC, IReflV, OTH) were first automatically converted to the new categories (ID -> VID, LVC -> LVC.cause/LVC.full, VPC -> IAV, IReflV -> IRV, OTH -> various categories). Next, the sentences were manually checked by two annotators in order to remove inconsistencies. An additional 2,100 sentences were annotated (5 packages of 400 sentences by a single annotator and 1 package of 100 sentences by all four annotators).

Tokenization
------------
* Language-specific tokenization rules are applied and have been manually checked in the corpus.

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 

Authors
----------
All VMWEs annotations were performed by Polona Gantar, Taja Kuzman, Teja Kavčič, Špela Arhar Holdt and Simon Krek. Support with data formatting and release was provided by Luka Terčon, Kaja Dobrovoljc and Jaka Čibej. 

License
----------
The data are distributed under the terms of the [Creative Commons BY-SA 4.0 license](https://creativecommons.org/licenses/by-sa/4.0/).

Contact
----------
simon.krek@ijs.si

Change log
----------
- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - Changes with respect to the 1.2 version are the following:
    - improving manually annotated lemmatization and JOS morphosyntactic tags
    - adding manually annotated UD part-of-speech tags and morphological features
    - adding UD dependency relations (see below for details)
- **2018-04-30**:
  - [Version 1.1](http://hdl.handle.net/11372/LRT-2842) of the corpus was released on LINDAT.
- **2017-01-20**:
  - [Version 1.0](http://hdl.handle.net/11372/LRT-2282) of the corpus was released on LINDAT.

